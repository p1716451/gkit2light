
//! \file tuto5.cpp utiliser mesh pour charger un objet .obj et le dessiner du point de vue d'une camera + controle de la camera a la souris

#include "window.h"

#include "mesh.h"
#include "wavefront.h"  // pour charger un objet au format .obj
#include "orbiter.h"

#include "draw.h"        // pour dessiner du point de vue d'une camera
#include <stdio.h>
#include "vec.h"

Mesh objet(GL_LINES);
Orbiter camera;


void  chaikin(std::vector<Point> & points){
  std::vector<Point> res;
  for(unsigned int i=0; i < points.size() - 1; i++){
    Point m1 = points[i]+ 0.25*Vector(points[i],points[i+1]);
    Point m2 = points[i]+ 0.75*Vector(points[i],points[i+1]);
    res.push_back(m1);
    res.push_back(m2);
  }
  points.clear();
  for(unsigned int i=0; i < res.size(); i++){
    points.push_back(res[i]);
  }
}


void affiche_cerlce(Mesh & m,std::vector<Point> cercle){
  for(unsigned int i=0; i < cercle.size() - 1; i++){
    m.vertex(cercle[i]);
    m.vertex(cercle[i+1]);
  }
  m.vertex(cercle[cercle.size()-1]);
  m.vertex(cercle[0]);
}


void affiche(Mesh & m, std::vector<Point> points, std::vector<Vector> orthogonaux){
  //J'affiche les points orthogonaux
  // for(unsigned int i=0; i < orthogonaux.size() ; i=i+10){
  //   m.vertex(points[i].x-orthogonaux[i].x, points[i].y-orthogonaux[i].y, points[i].z-orthogonaux[i].z);
  //   m.vertex(points[i]);
  // }

  // j'affiche le reste des points
  for(unsigned int i=0; i < points.size() - 1; i++){
    m.vertex(points[i]);
    m.vertex(points[i+1]);
  }
}


Vector rotation (Vector & v1, Vector & v2, Vector & d){
  Transform t = Rotation(v1,v2);
  return t(d);
}

void vecteur_orthogonal (std::vector<Point> & points, std::vector<Vector> & orthogonaux){
  Vector a0(points[0],points[1]);
  Vector v(1,0,0);
  Vector d = normalize(cross(a0,v))*0.1;
  orthogonaux.push_back(d);
  for(unsigned int i = 0; i < points.size() - 2; i++){
    Vector a0(points[i],points[i+1]);
    Vector a1(points[i+1], points[i+2]);
    d = rotation(a0, a1,d);
    orthogonaux.push_back(d);
  }
}


void points_cercle(Point & p1, Point & p2, Vector & v, std::vector<Point> & pc){
  Vector axe(p1,p2);
  float angle = 30.0;
  Transform t = Rotation(axe, angle);
  for(int i = 0; i < 12; i++){
    v = t(v);
    pc.push_back(Point(p1.x-v.x, p1.y-v.y, p1.z-v.z));
  }
}

void generation_cercles(std::vector<Point> & points, std::vector<Vector> & orthogonaux, std::vector<std::vector<Point>> & cercles){
  std::vector<Point> pc;
  for(unsigned int i = 0; i < orthogonaux.size() - 1; i++){
    pc.clear();
    points_cercle(points[i], points[i+1], orthogonaux[i], pc);
    cercles.push_back(pc);
  }
}

void dessin_triangles(Mesh & m,std::vector<std::vector<Point>> cercles){
  for(unsigned int i=0; i < cercles.size()-1; i++){
    for(unsigned int j = 0; j< cercles[i].size()-1; j++){
      m.vertex(cercles[i][j]);
      m.vertex(cercles[i+1][j]);
      m.vertex(cercles[i][j]);
      m.vertex(cercles[i+1][j+1]);
      if(j == cercles[i].size()-2){
        m.vertex(cercles[i][j+1]);
        m.vertex(cercles[i+1][j+1]);
        m.vertex(cercles[i][j+1]);
        m.vertex(cercles[i+1][0]);
      }
    }
  }
}


int init( )
{
    // etape 1 : charger un objet
    std::vector<Point> points;
    points.push_back(Point(0., 1., 0.));
    points.push_back(Point(0., 1., 1.));
    points.push_back(Point(1., 1., 1.));
    points.push_back(Point(1., 0., 1.));
    points.push_back(Point(0., 0., 1.));
    points.push_back(Point(0., 0., 0.));
    points.push_back(Point(1., 0., 0.));
    points.push_back(Point(1., 1., 0.));

    for(unsigned int i=0; i < 10; i++){
      chaikin(points);
    }


    std::vector<Vector> orthogonaux;
    std::vector<std::vector<Point>> cercles;

    //je construis les vecteurs orthogonaux à la courbe
    vecteur_orthogonal(points, orthogonaux);
    //génération des cercles
    generation_cercles(points, orthogonaux, cercles);
    // j'affiche tout les cercles
    for(unsigned int i = 0; i<cercles.size(); i = i+20){
      affiche_cerlce(objet, cercles[i]);
    }
    dessin_triangles(objet, cercles);
    //J'affiche le resultat
    affiche(objet, points, orthogonaux);






    // etape 2 : creer une camera pour observer l'objet
    // construit l'englobant de l'objet, les extremites de sa boite englobante
    Point pmin, pmax;
    objet.bounds(pmin, pmax);

    // regle le point de vue de la camera pour observer l'objet
    camera.lookat(pmin, pmax);

    // etat openGL par defaut
    glClearColor(0.2f, 0.2f, 0.2f, 1.f);        // couleur par defaut de la fenetre

    // etape 3 : configuration du pipeline.
/* pour obtenir une image correcte lorsque l'on dessine plusieurs triangles, il faut choisir lequel conserver pour chaque pixel...
    on conserve le plus proche de la camera, celui que l'on voit normalement. ce test revient a considerer que les objets sont opaques.
 */
    glClearDepth(1.f);                          // profondeur par defaut
    glDepthFunc(GL_LESS);                       // ztest, conserver l'intersection la plus proche de la camera
    glEnable(GL_DEPTH_TEST);                    // activer le ztest

    return 0;   // ras, pas d'erreur
}

int draw()
{
    // etape 2 : dessiner l'objet avec opengl

    // on commence par effacer la fenetre avant de dessiner quelquechose
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // on efface aussi le zbuffer

    // on dessine le triangle du point de vue de la camera
    //draw(objet, camera);

    //return 1;   // on continue, renvoyer 0 pour sortir de l'application

// on peut aussi controler la camera avec les mouvements de la souris


    // recupere les mouvements de la souris, utilise directement SDL2
    int mx, my;
    unsigned int mb= SDL_GetRelativeMouseState(&mx, &my);

    // deplace la camera
    if(mb & SDL_BUTTON(1))              // le bouton gauche est enfonce
        // tourne autour de l'objet
        camera.rotation(mx, my);

    else if(mb & SDL_BUTTON(3))         // le bouton droit est enfonce
        // approche / eloigne l'objet
        camera.move(mx);

    else if(mb & SDL_BUTTON(2))         // le bouton du milieu est enfonce
        // deplace le point de rotation
        camera.translation((float) mx / (float) window_width(), (float) my / (float) window_height());

    draw(objet, camera);

    return 1;

}

int quit( )
{
    // etape 3 : detruire la description du triangle
    objet.release();
    return 0;   // ras, pas d'erreur
}


int main( int argc, char **argv )
{
    // etape 1 : creer la fenetre
    Window window= create_window(1024, 640);
    if(window == NULL)
        return 1;

    // etape 2 : creer un contexte opengl pour pouvoir dessiner
    Context context= create_context(window);
    if(context == NULL)
        return 1;

    // etape 3 : creation des objets
    if(init() < 0)
    {
        printf("[error] init failed.\n");
        return 1;
    }

    // etape 4 : affichage de l'application, tant que la fenetre n'est pas fermee. ou que draw() ne renvoie pas 0
    run(window, draw);

    // etape 5 : nettoyage
    quit();
    release_context(context);
    release_window(window);
    return 0;
}
